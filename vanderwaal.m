function [v] = vanderwaal(P,T)
%Newton Raphson Solution for the Van der Waal Equation for Carbon Dioxide
%   First variable is pressure in atmospheres, the second is temperature in
%   Kelvin, if no temperature is input it will defualt to 293K
%   Answers given in L/Mol
%   Version 1.0 15/02/2019

if nargin==0, error('Not enough input arguments.'); end
if nargin < 2, T = 293; end
%defining constants
R = 0.0826;
a = 3.610; 
b = 0.043;
tolerance = 1E-6;
%initial guess using ideal gas law
v = (R*T)./P;
%number of iterations
N = 20;
%Newton Raphson
for count = 1:N
    if count == N+1
         error('Maximum number of iterations reached without an answer, answer will not converge')
    end
    
    f=(P+(a./v.^2)).*(v-b)-(R*T);
    fdash=P+(a./v.^2) - (2*a.*(v - b))./v.^3;
    v = v-((f)./(fdash));
    if(abs(f)<tolerance) 
        
        %close enough to converging to a root of f
        break; 
    end
end
