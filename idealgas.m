function [v] = idealgas(P,T)
%Newton Raphson solution to ideal gas law volume
%   Pressure,Temperature(P,T) used as inputs
%   Answers will be in L/Mol
%   Temperature defaults to 293K if no input is given
%   Version 2.0 22/02/2019
%       1.5 - Changed Description
%       2.0 - Added Maximum Interations check
if nargin==0, error('Not enough input arguments.'); end
if nargin < 2, T = 293; end
%defining constants
R = 0.0826;
%initial guess
v = 1;
%number of iterations
N = 20;
%Newton Raphson
for count = 1:N
    if count == N+1
         error('Maximum number of iterations reached without an answer, answer will not converge')
    end
    f = P.*v - R*T;
    fdash = P;
    if(abs(f)<0.00001) 
        %close enough to converging to a root of f
        break; 
    end
    v = v-((f)./(fdash));
end
